package br.com.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Stateless
@Entity
@Table(name = "PRODUCT")
@XmlRootElement
public class Product implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name = "parent_product_id")
	private Product parent_product_id;
	
	@OneToMany(mappedBy="parent_product_id")
	private Set<Product> childs = new HashSet<Product>();
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="product_id")
	private Set<Image> images = new HashSet<Image>();
		    
		
	public Product() {}
	
	public Product(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Product(int id, String name, String description, Product parent_product_id) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.parent_product_id = parent_product_id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getDescription() {
		return description;
	}

	
	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParent_product_id() {
		return parent_product_id;
	}

	public void setParent_product_id(Product parent_product_id) {
		this.parent_product_id = parent_product_id;
	}
	
	
}
