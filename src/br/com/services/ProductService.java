package br.com.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.entity.Product;
import br.com.repository.ProductRepository;

@Path("product")
public class ProductService {
		
	ProductRepository repository = new ProductRepository();
	
	@GET	
	@Produces(MediaType.APPLICATION_JSON)
	public String CheckRunning() {
		return "Product Service Running...";
	}
		
	
	@GET
	@Path("/GetAll")
	@Produces({MediaType.APPLICATION_JSON})
	public Response GetAllProducts() throws SQLException {
		List<Product> list = repository.GetAll();		
		return Response.ok()
				.entity(new GenericEntity<List<Product>>(list) {})
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
	}
	
	@GET
	@Path("/GetAllRelated")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> GetAllProductsAndRelated() {
		//call repository
		List<Product> list = new ArrayList<Product>();
		return  list;
	}
	
	@POST
	@Path("/GetById/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> GetById(@PathParam("id") Long id) {
		//call repository
		List<Product> list = new ArrayList<Product>();
		return  list;
	}
	
	@POST
	@Path("/productsrelated/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> GetByIdProductsAndRelated(@PathParam("id") Long id) {
		//call repository
		List<Product> list = new ArrayList<Product>();
		return  list;
	}
	
	@POST
	@Path("/GetChilds/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> GetChildsByParentId(@PathParam("id") Long id){
		//call repository
		List<Product> list = new ArrayList<Product>();
		return  list;
	}
}
