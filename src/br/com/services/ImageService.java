package br.com.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.entity.Image;
import br.com.repository.ImageRepository;

@Path("/image")
public class ImageService {

	ImageRepository repository = new ImageRepository();
	
	@GET	
	@Produces(MediaType.TEXT_PLAIN)
	public String CheckRunning() {
		return "Image Service Running...";
	}
	
	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Image> GetAllByProductId(@PathParam("id") Long id){
		List<Image> list = new ArrayList<Image>();
		return list;
	}
}
