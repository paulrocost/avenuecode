package br.com.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.h2.tools.Server;

import br.com.entity.Image;
import br.com.entity.Product;

public class h2hibernate extends ConnectionBase{

	public static void main(String[] args) throws SQLException {
		
		openServerModeInBrowser();
		populatetables();				
	}
	
	private static void populatetables() throws SQLException{
	        
          try {
        	transaction.begin();
        	
        	List<Product> products = new ArrayList<Product>();
        	products.add(new Product(1,"Tenis","Nike brand"));
        	products.add(new Product(2,"Camisa","Nike brand"));
        	products.add(new Product(3,"Cal�a","Nike brand"));
        	products.add(new Product(4,"Short","Nike brand"));
        	products.add(new Product(5,"Meia","Nike brand"));
        	products.add(new Product(6,"Cueca","Nike brand"));
        	products.add(new Product(7,"Bone","Nike brand"));
        	products.add(new Product(8,"Relogio","Nike brand"));
        	
        	for (Product item : products) {
        		item = entityManager.merge(item);
        		entityManager.flush();
        		entityManager.clear();
			}        	
        	        	                      	
        	entityManager.getTransaction().commit();
             
        	entityManager.getTransaction().begin();
             
             List<Image> images = new ArrayList<Image>();
             images.add(new Image(1,"jpg",1));
             images.add(new Image(2,"jpg",1));
             images.add(new Image(3,"jpg",2));
             images.add(new Image(4,"jpg",2));
             images.add(new Image(5,"jpg",3));
             images.add(new Image(6,"jpg",4));
             images.add(new Image(7,"jpg",5));
             
             for (Image item : images) {
         		item = entityManager.merge(item);
         		entityManager.flush();
         		entityManager.clear();
 			}   
             
             entityManager.getTransaction().commit();
             
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		} finally {
			entityManager.close();        	        
		}
   	}
	
	private static void openServerModeInBrowser() throws SQLException {
        Server server = Server.createTcpServer().start();
        System.out.println("Server started and connection is open.");
        System.out.println("URL: jdbc:h2:" + server.getURL() + "/mem:testdb");
    }

}
