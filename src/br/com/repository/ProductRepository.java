package br.com.repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import br.com.database.ConnectionBase;
import br.com.entity.Product;

public class ProductRepository extends ConnectionBase  {
			
	public List<Product> GetAll() throws SQLException{
		List<Product> list = new ArrayList<Product>();
		try {			
			 if(!transaction.isActive())
				 transaction.begin();
			 Query q1 = entityManager.createQuery("SELECT p FROM Product p");	
			 list = q1.getResultList();
			 
			 
		} catch (Exception e) {
			System.out.println("Exception Message " + e.getLocalizedMessage());
		}
		return list;
	}	
	
}
