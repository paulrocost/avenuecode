angular.module('app').service('productservice',['$http', function($http){
	var endpoint = function(directory, parameter){
		var response = 'http://localhost/AvenueFullstack/webapi/product/' + directory;
		
		if(parameter){
			response += parameter;
		}
		
		return response;
	}
	
	this.retrieveproductcollection = function(){
		var endpoint = endpoint('GetAll');
		return $http.get(endpoint);
	}
	
}]);