angular.module('app').controller('productcontroller',['$scope', '$http','productservice',
	function($scope,$http, productservice){
	
	$scope.collection = [];
	
	$scope.listall = function(){
		productservice.retrieveproductcollection()
			.success(function(response){
				if(response)
					$scope.collection = response;
			});
	}
	
}]);