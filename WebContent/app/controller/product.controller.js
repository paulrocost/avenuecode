angular.module('app').controller('productcontroller',['$scope', '$http','productservice','imageservice',
	function($scope,$http, productservice,imageservice){
	
	$scope.collection = [];
	
	$scope.listall = function(){
		productservice.retrieveproductcollection()
			.then(function(response){
				if(response)
					$scope.collection = response;
			});
	}
	
	
	$scope.listallrelated = function(){
		productservice.retrieveproductandrelatedcollection()
			.then(function(response){
				if(response)
					$scope.collection = response;
			})
	}
	
	$scope.listproductbyid = function(id){
		productservice.retrieveproductsbyid(id)
			.then(function(response){
				if(response)
					$scope.collection = response;
			})
	}
	
	$scope.listproductrelatedbyid = function(productid){
		productservice.retrieveproductrelatedbyid(productid)
			.then(function(response){
				if(response)
					$scope.collection = response;
			})
	}
	
	$scope.childsbyproductid = function(id){
		productservice.retrievechildsbyid(id)
			.then(function(response){
				if(response)
					$scope.collection = response;
			})
	}
	
	$scope.imagesbyproductid = function(id){
		imageservice.retrieveimagesbyproductid(id)
			.then(function(response){
				if(response)
					$scope.collection = response;
			})
	}
	
}]);