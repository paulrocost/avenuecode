angular.module('app').service('productservice',['$http', function($http){
	var endpointapi = function(directory, parameter){
		var response = 'http://localhost:8080/AvenueFullstack/webapi/product/' + directory;
		
		if(parameter){
			response += parameter;
		}
		
		return response;
	}
	
	this.retrieveproductcollection = function(){
		var endpoint = endpointapi('GetAll');
		return $http.get(endpoint);
	}
	
	this.retrieveproductandrelatedcollection = function(){
		var endpoint = endpointapi('GetAllRelated');
		return $http.get(endpoint);
	}
	
	this.retrieveproductsbyid = function(id){
		var endpoint = endpointapi('GetById',id);
		return $http.post(endpoint);
	}
	
	this.retrieveproductrelatedbyid = function(id){
		var endpoint = endpointapi('productsrelated',id);
		return $http.post(endpoint);
	}
	
	this.childsbyproductid = function(id){
		var endpoint = endpointapi('productsrelated',id);
		return $http.post(endpoint);
	}
	
}]);