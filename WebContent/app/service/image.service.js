angular.module('app').service('imageservice',['$http', function($http){
	var endpointapi = function(directory, parameter){
		var response = 'http://localhost/AvenueFullstack/webapi/image/' + directory;
		
		if(parameter){
			response += parameter;
		}
		
		return response;
	}
	
	this.retrieveimagesbyproductid = function(id){
		var endpoint = endpointapi('',id);
		return $http.post(endpoint);
	}
		
	
}]);